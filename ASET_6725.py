class Graph:
  def __init__(self):
    self.nodes = set()
    self.edges = dict()
    self.distances = {}

  def add_node(self, value):
    self.nodes.add(value)
    #print("Node added: ", value)

  def add_edge(self, from_node, to_node, distance):
    self.edges[from_node] = to_node
    self.edges[to_node] = from_node
    self.distances[(from_node, to_node)] = distance


def dijsktra(graph, initial):
  visited = {initial: 0}
  path = {}

  nodes = set(graph.nodes)

  while nodes: 
    min_node = None
    for node in nodes:
      if node in visited:
        if min_node is None:
          min_node = node
        elif visited[node] < visited[min_node]:
          min_node = node

    if min_node is None:
      break

    nodes.remove(min_node)
    current_weight = visited[min_node]

    for edge in graph.edges[min_node]:
      weight = current_weight + graph.distances[(min_node, edge)]
      if edge not in visited or weight < visited[edge]:
        visited[edge] = weight
        path[edge] = min_node

  return visited, path


g = Graph()
g.add_node('a')
g.add_node('b')
g.add_node('c')
g.add_node('d')

g.add_edge('a', 'b', 10)
g.add_edge('b', 'c', 10)
g.add_edge('a', 'c', 15)
g.add_edge('c', 'd', 20)
g.add_edge('d', 'c', 10)

#print(g.nodes, "\n", g.edges, '\n', g.distances)

print('-'*20)
print("Coded by Mrinal Wahal")
print('-'*20)
print(dijsktra(g, 'a'))
